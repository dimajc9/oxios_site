import React, { useEffect, useState } from 'react'
import { BrowserRouter as Router, Link, Route, Switch } from 'react-router-dom'

import Button from '../../components/button/button'

import './header.style.css'

import unnamed from './img/unnamed.png'
import logo from './img/logo.png'
import hamburger from './img/hamburger.png'

export const Header = () => {
    const [click, setClick] = useState(false)

    const handleClick = () => {
        setClick((prevState) => {
            setClick(!prevState)
        })
    }
    console.log(click);
   
    

    return(
        <div className='main_block'>
                {click && <div className='modalHamburger'>
                    <div className='modalHamburger_unnamed'>
                        <img src={unnamed} alt="" className='modalHamburger_unnamed_img' onClick={handleClick}/>
                    </div>
                    <div className='modalHamburger_text'>
                        <Link to='/' onClick={handleClick}>Our Works</Link>
                        <Link to='/' onClick={handleClick}>Technologies</Link>
                        <Link to='/' onClick={handleClick}>About Us</Link>
                        <Link to='/' onClick={handleClick}>Blog</Link>
                    </div>
                </div> }
            <header className='header'>
                <img src={logo} alt="img" className='img_logo'/>
                <img src={hamburger} alt="" className='hamburger' onClick={handleClick}/>
                <div className='header_block'>
                    <Link to='/'>Our Works</Link>
                    <Link to='/'>Technologies</Link>
                    <Link to='/'>About Us</Link>
                    <Link to='/'>Blog</Link>
                </div>
                <Button text='Hire Us' className='btn'/>
            </header>
        </div>
    )
}