import React from 'react'
import { BrowserRouter as Router, Link, Route, Switch } from 'react-router-dom'

import { Header } from '../header/header'
import Button from '../../components/button/button'

import './main.style.css'

import car1 from './img/car 1.png'
import phoneSamsung from './img/Samsung Clay.png'
import group from './img/Group 6.png'
import iphoneX from './img/iPhone X Dark.png'
import logoFooter from './img/logo_footer.png'
import twitter from './img/Twitter_white.png'
import instagram from './img/Instagram_white.png'
import facebook from './img/Facebook_white.png'
import linkedin from './img/LinkedIN_white.png'


export const Main = () => {
    return(
        <div className='main'>
            <Header />
            <div className="main_info_block">
                <div className="block_center_car">
                    <div className='text'>
                        <p>Garage APP - Все СТО Харькова и Киева в твоём кармане</p>
                    </div>
                    <div className="block_img_car">
                        <img src={car1} alt="" className='car1'/>
                    </div>
                </div>
                <div className='block_center_technologies'>
                    <div className="technologies_info">
                        <p className='text_cap'>Технологии реализации:</p>
                        <p className='text_bac'>- Backend</p>
                        <div className="technologies_info_bac">
                            <Button className='noda'/>
                            <Button className='mongo'/>
                        </div>
                        <p className='text_front'>- Frontend</p>
                        <Button className='front'/>
                        <div className='buttons'>
                            <Button className='toCite' text='Перейти на сайт'/>
                            <Button className='store' text='App store'/>
                            <Button className='play' text='Google play'/>
                        </div>
                    </div>
                </div>
            </div>
            <div className='block_for_users'>
                <p className='users_cap'>Для пользователей приложения</p>
                <div className="users_block_info">
                    <div className="blocks_to_user">
                        <div className="strip"></div>
                        <div className="block_text">
                            <p className='block_info_cap'>Скорость</p>
                            <p className='block_info_text'>Работа приложения расчитана на максимальную производительность , оно будет “летать” даже на самых старых девайсах</p>
                        </div>
                    </div>
                    <div className="blocks_to_user">
                        <div className="strip"></div>
                        <div className="block_text">
                            <p className='block_info_cap'>Надежность</p>
                            <p className='block_info_text'>Ваши данные в полной безопасности!</p>
                        </div>
                    </div>
                    <div className="blocks_to_user">
                        <div className="strip"></div>
                        <div className="block_text">
                            <p className='block_info_cap'>Карта поиска и push-уведомления</p>
                            <p className='block_info_text'>Мы даём возможность найти на карте ближайшие сто и записаться на любые услуги. Так же вы можете проложить оптимальный маршрут к вашему мастеру.</p>
                        </div>
                    </div>
                    <div className="blocks_to_user">
                        <div className="strip"></div>
                        <div className="block_text">
                            <p className='block_info_cap'>Кроссплатформеность</p>
                            <p className='block_info_text'>Приложение доступно на любом вашем мобильном устройстве.</p>
                        </div>
                    </div>
                </div>
                <div className="block_phones">
                    <img src={phoneSamsung} alt="img" className='phone'/>
                    <img src={iphoneX} alt="img" className='phone'/>
                    <img src={group} alt="img" className='phone'/>
                    <img src={iphoneX} alt="img" className='phone'/>
                </div>
            </div>
            <div className='for_owners'>
                <p className='for_owners_cap'>Для владельцев СТО</p>
                <div className='block_info'>
                    <div className='block_info_cap'>
                        <p className='cap_text'>Удобная админ панель, телеграм бот и многое другое</p>
                    </div>
                    <div className='block_info_text'>
                        <p className='text'>Для владельцев СТО создано WEB- приложение с возможностью управлять всеми своими сервисами.</p>
                        <p className='text'>Телеграм бот который будет всегда держать вас в курсе в ходе появления или изменения заказов</p>
                    </div>
                    <Button text='Владеете СТО? Попробуйте Garage' className='for_owners-btn'/>
                </div>
            </div>
            <div className='bottom_project'>
                    <p className='bottom_project_cap'>Do you wnat to discuss your project?</p>
                    <Button className='bottom_project_btn' text='Contact Us'/>
            </div>
            <footer className='footer'>
                <div className='footer_logo'>
                    <img src={logoFooter} alt="img" className='footer_logo-img'/>
                    <p className='' className='footer_logo-p'>OXIOS © 2020  All Rights Reserved</p>
                </div>
                <div className='footer_links'>
                    <p className='footer_links_cap'>Links</p>
                    <div className='footer_links_link'>
                        <Link to='/'>Our works</Link>
                        <Link to='/'>Technologies</Link>
                        <Link to='/'>About Us</Link>
                        <Link to='/'>Blog</Link>
                        <Link to='/'>Contact Us</Link>
                        <Link to='/'>Get a Checklist</Link>
                    </div>
                </div>
                <div className='footer_follow'>
                    <p className='footer_follow_cap'>Follow Us</p>
                    <div className='follow'>
                        <img src={twitter} alt="img" className='follow_img'/>
                        <a href='#' className='follow_name'>Twitter</a>
                    </div>
                    <div className='follow'>
                        <img src={instagram} alt="img" className='follow_img'/>
                        <a href='#' className='follow_name'>Instagram</a>
                    </div>
                    <div className='follow'>
                        <img src={facebook} alt="img" className='follow_img'/>
                        <a href='#' className='follow_name'>Facebook</a>
                    </div>
                    <div className='follow'>
                        <img src={linkedin} alt="img" className='follow_img'/>
                        <a href='#' className='follow_name'>Linkedin</a>
                    </div>
                </div>
            </footer>
        </div>
    )
}