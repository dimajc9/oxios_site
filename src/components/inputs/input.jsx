import React from 'react'

import './inputs.style.css'

class Input extends React.Component {
    render(){
        return(
            <input 
                className={this.props.className}
                type={this.props.type} 
                placeholder={this.props.placeholder}
                onChange={this.props.handleChange}
                value={this.props.value}
            />
        )
    }
}

export default Input