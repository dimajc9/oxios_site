import React from 'react'

import './button.style.css'

class Button extends React.Component {
    render(){
        return(
            <button 
                id={this.props.id}
                className={this.props.className}
                onClick={this.props.handleClick}>
                    {this.props.text}
            </button>
        )
    }
}

export default Button
